<?php

use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

Route::get('/', [AuthenticationController::class, 'guest'])->middleware('guest');
Route::get('/login', [AuthenticationController::class, 'login'])->name('login')->middleware('guest');
Route::post('/login', [AuthenticationController::class, 'doLogin']);
Route::get('/register', [AuthenticationController::class, 'register'])->name('register')->middleware('guest');
Route::post('/register', [AuthenticationController::class, 'doRegister']);
Route::get('/logout', [AuthenticationController::class, 'logout']);

Route::middleware(['auth'])->group(function () {
    Route::get('/home', [DashboardController::class, 'index'])->name('transactions.dashboard');
    Route::get('/topup', [TransactionController::class, 'topup'])->name('transactions.topup');
    Route::get('/topup/add', [TransactionController::class, 'topupCreate'])->name('transactions.topup-create');
    Route::post('/topup', [TransactionController::class, 'topupSave'])->name('transactions.topup-save');
    Route::get('/transaction', [TransactionController::class, 'transaction'])->name('transactions.out');
    Route::get('/transaction/add', [TransactionController::class, 'transactionCreate'])->name('transactions.out-create');
    Route::post('/transaction', [TransactionController::class, 'transactionSave'])->name('transactions.out-save');
    Route::get('/history', [TransactionController::class, 'history'])->name('transactions.history');
});