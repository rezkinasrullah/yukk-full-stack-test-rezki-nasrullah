@extends("backend.layouts.master")

@section("title", "Login App")

@section("body")
<section class="section">
    <div class="container mt-5">
      <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
          <div class="login-brand">
            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/LOGO_YUKK.svg/1280px-LOGO_YUKK.svg.png" alt="logo" width="100" class="shadow-light rounded-circle">
          </div>

          <div class="card card-primary">
            <div class="card-header"><h4>Login</h4></div>

            <div class="card-body">
              @if(session()->has("success"))
              <div class="alert alert-primary">
                  {{ session()->get("success") }}
              </div>
              @endif
              @if(session()->has("error"))
              <div class="alert alert-danger">
                  {{ session()->get("error") }}
              </div>
              @endif
              <form method="POST" action="" class="needs-validation" novalidate="">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="email">Email</label>
                  <input id="email" type="text" class="form-control" name="email" tabindex="1" required autofocus>
                  <div class="invalid-feedback">
                    Masukkan Email
                  </div>
                </div>

                <div class="form-group">
                  <div class="d-block">
                      <label for="password" class="control-label">Password</label>
                  </div>
                  <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                  <div class="invalid-feedback">
                    Masukkan password anda
                  </div>
                </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                    Login
                  </button>
                </div>
              </form>
            </div>
          </div>
          <div class="mt-5 text-muted text-center">
            Don't have an account? <a href="{{ route('register') }}">Create One</a>
          </div>
          <div class="simple-footer">
            Copyright &copy; Stisla 2018
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
