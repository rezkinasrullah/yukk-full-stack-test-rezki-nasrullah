@extends('backend.layouts.app')

@section("content")
<link rel="stylesheet" href="{{ url('assets/combogrid') }}/jquery.bs-combogrid.min.css">
<div class="section-header no-print">
    <h1>@yield("title")</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
      <div class="breadcrumb-item"><a href="#">@yield("title")</a></div>
    </div>
  </div>
  <div class="section-body">
    <h2 class="section-title  no-print">@yield("title")</h2>
    <p class="section-lead  no-print">@yield("keterangan")</p>
    @yield("isi")
  </div>
@endsection

@section('page_script')
    <script src="{{ url('assets/combogrid') }}/jquery.bs-combogrid.min.js"></script>
    <script src="{{ url('/') }}/js/accounting.min.js"></script>
    <script src="{{ url('/') }}/js/clipboard.min.js"></script>
    @yield("script")
@endsection
