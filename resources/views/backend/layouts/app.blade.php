@extends("backend.layouts.master")

@section("body")
<div class="main-wrapper">
    <div class="navbar-bg"></div>
    <nav class="navbar navbar-expand-lg main-navbar">
      <form class="form-inline mr-auto">
        <ul class="navbar-nav mr-3">
          <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
        </ul>
      </form>
      <ul class="navbar-nav navbar-right">
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
          <img alt="image" src="{{ url('/') }}/assets/img/avatar/avatar-1.png" class="rounded-circle mr-1">
          <div class="d-sm-none d-lg-inline-block">Hi, {{ auth()->user()->name ?? '' }}</div></a>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-title">Logged in 5 min ago</div>
            <div class="dropdown-divider"></div>
            <a href="{{ url('logout') }}" class="dropdown-item has-icon text-danger">
              <i class="fas fa-sign-out-alt"></i> Logout
            </a>
          </div>
        </li>
      </ul>
    </nav>
    <div class="main-sidebar no-print">
      <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
          <a href="#">Yukk</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
          <a href="#">Y</a>
        </div>
        <ul class="sidebar-menu">
            <li><a class="nav-link" href="{{ url('home') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>
           
            <li class="dropdown">
              <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-archive"></i> <span>Transaksi</span></a>
              <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ url("topup") }}">Topup</a></li>
                <li><a class="nav-link" href="{{ url("transaction") }}">Transaksi</a></li>
              </ul>
            </li>
            <li><a class="nav-link" href="{{ url('history') }}"><i class="fas fa-archive"></i> <span>History</span></a></li>

            
           
        </ul>

          {{-- <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
              <i class="fas fa-rocket"></i> Documentation
            </a>
          </div> --}}
      </aside>
    </div>

    <!-- Main Content -->
    <div class="main-content">
      <section class="section">
         @yield("content")
      </section>
    </div>
    <footer class="main-footer no-print">
      <div class="footer-left">
        Copyright &copy; 2018
      </div>
      <div class="footer-right">
        2.3.0
      </div>
    </footer>
  </div>
@endsection

@section('page_script_master')
    <script>
      var start = Date.now();
      var theInterval = null;
      // auto logout algorithm
      if (typeof session_time !== 'undefined' && typeof alert_time !== 'undefined') {
          // the variable is defined
          x = session_time;
          y = alert_time;
      }else {
          x = 1200000; // 20 menit
          y = 900000; // 15 menit
      }
      function setIntervalLogout(){
          if(theInterval !== null){
            clearInterval(theInterval);
          }
          theInterval = setInterval(function () {
              if (Date.now() - start > x) {
                  clearInterval(theInterval);
                  location.href = '{{ url('logout') }}';
                  return;
              }
              if(Date.now() - start > y){ // menit ke 4 popup
                swal({
                    title: "Anda tidak beraktivitas?",
                    text: "Sepertinya anda tidak beraktivitas selama 4 menit, apakah anda masih ingin menggunakan aplikasi ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        start = Date.now();
                        setIntervalLogout();
                    }
                });
              }
          }, 60000);
      }

      $(function() {
         setIntervalLogout();
         $('.datetimepickerz').daterangepicker({
          locale: {format: 'YYYY-MM-DD HH:mm:ss'},
          singleDatePicker: true,
          timePicker: true,
          timePicker24Hour: true,
        });
      });
      
    </script>
    @yield("page_script")
@endsection