@extends('backend.layouts.crud')

@section("title", "Topup")

@section("keterangan", "Topup")

@section("isi")
            <div class="row">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                    <h4>Topup</h4>

                      </div>
                  <div class="card-body">
                    @if($errors->any())
                    <div class="alert alert-danger">
                        {!!  implode('<br>', $errors->all('<div>:message</div>')) !!}
                    </div>
                    @endif
                    <form action="{{ url('topup') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                   
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <input type="text" class="form-control" name="description" placeholder="Keterangan">
                        </div>
                        <div class="form-group">
                            <label>Total</label>
                            <input type="number" class="form-control" name="amount" placeholder="Total">
                        </div>
                        <div class="form-group">
                            <label>Bukti Transfer</label>
                            <input type="file" name="proof" class="form-control">
                        </div>
                      
                    <hr>
                    <button class="btn btn-primary pull-right">Simpan</button>
                    </form>
                    <hr>
                  </div>
                </div>

              </div>

            </div>
@endsection