@extends('backend.layouts.crud')

@section("title", "Transaction")

@section("keterangan", "Transaction")

@section("isi")
            <div class="row">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Transaction</h4>
                        <div class="card-header-action">
                            <a href="{{ url('transaction/add') }}" class="btn btn-primary">Add Transaction <i class="fas fa-chevron-right"></i></a>
                          </div>
                    </div>
                  <div class="card-body">
                    @if(session()->has("success"))
                    <div class="alert alert-primary">
                        {{ session()->get("success") }}
                    </div>
                    @endif
                    @if(session()->has("error"))
                    <div class="alert alert-danger">
                        {{ session()->get("error") }}
                    </div>
                    @endif
                    <div class="table-responsive table-invoice">
                        <form action="" method="get">
                            <div class="form-group">
                                <label>Cari Berdasarkan Deskripsi/Kode Transaksi</label>
                                
                                <input name="q" type="text" class="form-control" placeholder="Ketikkan sesuatu lalu tekan enter">
                            </div>
                        </form>
                        <hr>
                        <table class="table table-striped">
                            <tr>
                              <th>No.</th>
                              <th>Kode</th>
                              <th>Deskripsi</th>
                              <th>Jumlah</th>
                              <th>Tanggal</th>
                            </tr>
                            @foreach ($transactions->items() as $key => $item)
                                <tr>
                                    <td>{{ ($key+1) }}</td>
                                    <td class="font-weight-600">{{ $item->transaction_code }}</td>
                                    <td>{{ $item->description }}</td>
                                    <td><div class="badge badge-{{ $item->amount >= 0 ? "success" : "danger"}}">{{ number_format($item->amount) }}</div></td>
                                    <td>{{ date("d F Y", strtotime($item->created_at)) }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <hr>
                    @if(request()->has("q"))
                        {!! $transactions->appends(['q' => request()->input("q")])->links() !!}
                    @else
                        {!! $transactions->links() !!}
                    @endif
                  </div>
                </div>

              </div>

            </div>
@endsection
