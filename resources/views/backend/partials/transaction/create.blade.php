@extends('backend.layouts.crud')

@section("title", "Transaction")

@section("keterangan", "Transaction")

@section("isi")
            <div class="row">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                    <h4>Transaction</h4>

                      </div>
                  <div class="card-body">
                    @if($errors->any())
                    <div class="alert alert-danger">
                        {!!  implode('<br>', $errors->all('<div>:message</div>')) !!}
                    </div>
                    @endif
                    <form action="{{ url('transaction') }}" method="POST" enctype="multipart">
                        @csrf
                   
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <input type="text" class="form-control" name="description" placeholder="Keterangan">
                        </div>
                        <div class="form-group">
                            <label>Total</label>
                            <input type="number" class="form-control" name="amount" placeholder="Total">
                        </div>
                      
                    <hr>
                    <button class="btn btn-primary pull-right">Simpan</button>
                    </form>
                    <hr>
                  </div>
                </div>

              </div>

            </div>
@endsection