@extends("backend.layouts.app")

@section("content")
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2">
     
          <div class="card-icon shadow-primary bg-primary">
            <i class="fas fa-dollar-sign"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Current Balance</h4>
            </div>
            <div class="card-body">
              {{  $userSaldo }}
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2">
     
          <div class="card-icon shadow-primary bg-primary">
            <i class="fas fa-dollar-sign"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Total Topup</h4>
            </div>
            <div class="card-body">
              {{ $userTopup }}
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-12">
        <div class="card card-statistic-2">
     
          <div class="card-icon shadow-primary bg-primary">
            <i class="fas fa-dollar-sign"></i>
          </div>
          <div class="card-wrap">
            <div class="card-header">
              <h4>Total Transaction</h4>
            </div>
            <div class="card-body">
              {{ $userTransaction }}
            </div>
          </div>
        </div>
      </div>
</div>
<div class="row">
  <div class="col-md-8">
    <div class="card">
      <div class="card-header">
        <h4>Last 10 Transactions/Topup</h4>
        <div class="card-header-action">
          <a href="{{ url('history') }}" class="btn btn-danger">View More</a>
        </div>
      </div>
      <div class="card-body p-0">
        <div class="table-responsive table-invoice">
          <table class="table table-striped">
            <tbody><tr>
              <th>Inv</th>
              <th>Description</th>
              <th>Amount</th>
              <th>Date</th>
            </tr>
            @foreach ($userJournal as $item)
            <tr>
              <td><a href="#">{{ $item->code }} - {{ $item->type }}</a></td>
              <td class="font-weight-600">{{ $item->description }}</td>
              <td><div class="badge badge-{{ $item->amount >= 0 ? "success" : "danger"}}">{{ number_format($item->amount) }}</div></td>
              <td>{{ date("d F Y", strtotime($item->created_at)) }}</td>
            
            </tr>
            @endforeach
           

          </tbody></table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card card-hero">
      <div class="card-header">
        <div class="card-icon">
          <i class="far fa-question-circle"></i>
        </div>
        <h4>14</h4>
        <div class="card-description">Customers need help</div>
      </div>
      <div class="card-body p-0">
        <div class="tickets-list">
          <a href="#" class="ticket-item">
            <div class="ticket-title">
              <h4>My order hasn't arrived yet</h4>
            </div>
            <div class="ticket-info">
              <div>Laila Tazkiah</div>
              <div class="bullet"></div>
              <div class="text-primary">1 min ago</div>
            </div>
          </a>
          <a href="#" class="ticket-item">
            <div class="ticket-title">
              <h4>Please cancel my order</h4>
            </div>
            <div class="ticket-info">
              <div>Rizal Fakhri</div>
              <div class="bullet"></div>
              <div>2 hours ago</div>
            </div>
          </a>
          <a href="#" class="ticket-item">
            <div class="ticket-title">
              <h4>Do you see my mother?</h4>
            </div>
            <div class="ticket-info">
              <div>Syahdan Ubaidillah</div>
              <div class="bullet"></div>
              <div>6 hours ago</div>
            </div>
          </a>
          <a href="features-tickets.html" class="ticket-item ticket-more">
            View All <i class="fas fa-chevron-right"></i>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section("page_script")
<script src="{{ url('node_modules/chart.js/dist/Chart.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js"></script>

@endsection
