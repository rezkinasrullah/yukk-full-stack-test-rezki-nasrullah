@extends('backend.layouts.crud')

@section("title", "History Transaction")

@section("keterangan", "History Transaction")

@section("isi")
            <div class="row">
              <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>History Transaction</h4>
                    </div>
                  <div class="card-body">
                    @if(session()->has("error"))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger">
                                {{ session()->get("error") }}
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="table-responsive table-invoice">
                        <form action="" method="get">
                            <div class="form-group">
                                <label>Cari Berdasarkan Deskripsi/Kode Transaksi</label>
                                
                                <input name="q" type="text" class="form-control" placeholder="Ketikkan sesuatu lalu tekan enter">
                            </div>
                        </form>
                        <hr>
                        <table class="table table-striped">
                            <tr>
                              <th>No.</th>
                              <th>Kode</th>
                              <th>Deskripsi</th>
                              <th>Jumlah</th>
                              <th>Tanggal</th>
                              <th>Tipe</th>
                            </tr>
                            @foreach ($histories->items() as $key => $item)
                                <tr>
                                    <td>{{ ($key+1) }}</td>
                                    <td class="font-weight-600">{{ $item->code }}</td>
                                    <td>{{ $item->transaction_description }}</td>
                                    <td><div class="badge badge-{{ $item->amount >= 0 ? "success" : "danger"}}">{{ number_format($item->amount) }}</div></td>
                                    <td>{{ date("d F Y", strtotime($item->created_at)) }}</td>
                                    <td align="center">{{ $item->type }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    <hr>
                    @if(request()->has("q"))
                        {!! $histories->appends(['q' => request()->input("q")])->links() !!}
                    @else
                        {!! $histories->links() !!}
                    @endif
                  </div>
                </div>

              </div>

            </div>
@endsection
