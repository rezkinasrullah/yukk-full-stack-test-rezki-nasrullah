
## Laravel Test in Yukk

Aplikasi untuk melakukan topup dan transaksi. Aplikasi membutuhkan registrasi awal untuk mendaftar sebagai user, setelah daftar atau login user akan disajikan dengan menu topup.

Catatan:
 - User saldo harus ada ketika ingin melakukan "transaksi"
 - Oleh sebab itu jika saldo 0 dan ingin melakukan transaksi akan ada popup untuk mengisi saldo
 - Ketika mengisi saldo wajib mengupload file bukti berupa image jadi mime type nya hanya image file saja
 - Untuk topup dan transaksi dianggap sudah diapprove admin

## Penjelasan 

Kenapa topup dan transaction dipisah? agar memudahkan ketika mencari data khusus seperti topup saja, dan transaction saja, dan juga memisahkan business logic apabila nantinya ada yang memakai salah satu saja. Dan apabila ingin melakukan rekonsiliasi atau log tracing dari transaksi dan topup maka kita sediakan juga table journal, gunanya semua log masuk dan keluar saldo akan ter rekap disini beserta dengan ref banknya (apabila ada), serta optional data semisal jika ada. Nanti ketika menghitung ulang saldo akan bisa melalui table journal saja.

Dengan pemisahan table transaksi, kita bisa melakukan data analytics kedepannya dengan table transaction saja (tanpa tercampur dengan topup)

## Project Documentation

### Require 
- php 8.2
- laravel 11
- mariadb

### How to install
After project on local this, open terminal then 

`composer install` 

Then copy .env.example to .env
Then generate app key with command 

`php artisan key:generate`

Fill up the env details (just database in this case)
Then run
`php artisan migrate:fresh --seed` 

That command will be creating fresh table from the oven, and seed the data from the seeder

### How to running
Run command 
`php artisan serve`

### Run Test
Run command 
`php artisan test`

## Architecture Details

This project using semi Repository Pattern, because i think repository is handled by Laravel Eloquent Model so we dont need to create Respository anymore, but still the business logic is in Service Class.