<?php

namespace App\Http\Services;


use Exception;

class UtilityService
{

   public function createNewTopupNumber(){
       return "TOPUP-".date("YmdHis");
   }

   public function createNewTransactionNumber(){
         return "TRX-".date("YmdHis");
   }

   public function numberFormat($number){
      return number_format($number, 0);
   }

   function numberDashboard($x) {
      if($x < 100000)
         return number_format($x, 0);
      elseif($x < 1000000)
         return (int)($x/1000). " K";
      elseif($x < 1000000000)
         return (int)($x/1000000). " M";
      elseif($x < 1000000000000)
         return (int)($x/1000000000). " B";
      else 
         return (int)($x/1000000000000). " T";
   }
}
