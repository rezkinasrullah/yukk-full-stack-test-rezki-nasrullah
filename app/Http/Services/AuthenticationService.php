<?php

namespace App\Http\Services;

use App\Models\User;
use Exception;

class AuthenticationService
{

    public function login($credentials){

        
        if(auth()->attempt($credentials)) return redirect('/home');

        return redirect('/login')->with('error', 'Maaf detail yang anda masukkan salah');
    }

    public function guest(){
        return redirect('login');
    }

    public function register($credentials){

        try{
            if($credentials['password'] != $credentials['confirmation_password']){
                throw new Exception("Maaf password dan password konfirmasi harus sama");
            }

            $user = new User();
            $user->name = $credentials['name'];
            $user->email = $credentials['email'];
            $user->password = bcrypt($credentials['password']);
            $user->save();


            // auto login after register
            auth()->login($user);

            return redirect('home');
        }catch(Exception $ex){
            return redirect('/register')->with('error', $ex->getMessage());
        }
    }

    public function logout(){
        auth()->logout();

        return redirect('/login')->with('success', 'Anda berhasil logout');
    }
}
