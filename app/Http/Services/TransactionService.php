<?php

namespace App\Http\Services;

use App\Models\Journal;
use App\Models\SaldoUser;
use App\Models\Topup;
use App\Models\Transaction;
use Exception;
use Illuminate\Support\Facades\DB;

class TransactionService
{
    protected $utilityService;


    public function __construct(UtilityService $utilityService)
    {
        $this->utilityService = $utilityService;
    }


    public function getCurrentUserBalance(){
        return auth()->user()->saldo->balance ?? 0;
    }

    public function getCurrentUserTotalTopup(){
        return Topup::where('user_id', auth()->id())->sum('amount');
    }

    public function getCurrentUserTotalTransaction(){
        return Transaction::where('user_id', auth()->id())->sum('amount');
    }

    public function getCurrentUserLatestJournal($limit = 10){
        return Journal::where('journal.user_id', auth()->id())
                        ->leftJoin('topup', function($join){
                            $join->on('topup.id', 'journal.transaction_id')->where('journal.type', 'topup');
                        })
                        ->leftJoin('transaction', function($join){
                            $join->on('transaction.id', 'journal.transaction_id')->where('journal.type', 'transaction');
                        })
                        ->select("journal.*", DB::raw("IFNULL(topup.transaction_code, transaction.transaction_code) as code"))
                        ->take($limit)
                        ->get();
    }

    public function getCurrentUserHistory($search = null, $limit = 10){
        return Journal::where('journal.user_id', auth()->id())
                        ->leftJoin('topup', function($join){
                            $join->on('topup.id', 'journal.transaction_id')->where('journal.type', 'topup');
                        })
                        ->leftJoin('transaction', function($join){
                            $join->on('transaction.id', 'journal.transaction_id')->where('journal.type', 'transaction');
                        })
                        ->select("journal.*", DB::raw("IFNULL(topup.transaction_code, transaction.transaction_code) as code"), DB::raw("IFNULL(topup.description, transaction.description) as transaction_description"))
                        ->when($search, function($q, $search){
                            $q->where('topup.transaction_code', 'LIKE', "%$search%")
                            ->orWhere('transaction.transaction_code', 'LIKE', "%$search%")
                            ->orWhere('topup.description', 'LIKE', "%$search%")
                            ->orWhere('transaction_description.description', 'LIKE', "%$search%");
                        })
                        ->paginate($limit);
    }

    public function getCurrentUserTopup($search = null, $limit = 10){
        return Topup::where('user_id', auth()->id())
                         ->when($search, function($q, $search){
                            $q->where('topup.transaction_code', 'LIKE', "%$search%")
                            ->orWhere('topup.description', 'LIKE', "%$search%");
                        })
                        ->paginate($limit);
    }

    public function getCurrentUserTransaction($search = null, $limit = 10){
        return Transaction::where('user_id', auth()->id())
                         ->when($search, function($q, $search){
                            $q->where('transaction.transaction_code', 'LIKE', "%$search%")
                            ->orWhere('transaction.description', 'LIKE', "%$search%");
                        })
                        ->paginate($limit);
    }


    public function saveToptup($topupDto, $request){
        try{
            DB::beginTransaction();

            $file = $request->file('proof');
            $destinationPath = public_path('uploads');

            if (!file_exists($destinationPath)) {
                mkdir($destinationPath, 0755, true);
            }

            $fileName = $file->getClientOriginalName();
            $file->move($destinationPath, $fileName);

            // Generate the URL to access the file
            $fileUrl = url('uploads/' . $fileName);
            
            $topup = new Topup();
            $topup->transaction_code = $this->utilityService->createNewTopupNumber();
            $topup->description = $topupDto['description'];
            $topup->amount = $topupDto['amount'];
            $topup->proof = $fileUrl;
            $topup->user_id = auth()->id();
            $topup->save();

            // save journal to audit trail 
            $journal = new Journal();
            $journal->transaction_id = $topup->id;
            $journal->type = 'topup';
            $journal->description = $topupDto['description'];
            $journal->amount = $topupDto['amount']; 
            $journal->user_id = auth()->id();
            $journal->save();

            // update saldo User 

            $saldoUser = SaldoUser::where('user_id', auth()->id())->first();

            if(!empty($saldoUser->id)){
                $saldoUser = SaldoUser::find($saldoUser->id);
                $saldoUser->balance +=  $topupDto['amount'];
                $saldoUser->save();
            }else{
                $saldoUser = new SaldoUser();
                $saldoUser->user_id = auth()->id();
                $saldoUser->balance = $topupDto['amount'];
                $saldoUser->save();
            }

            DB::commit();
            return redirect('topup')->with('success', 'Success add topup');
        }catch(Exception $ex){
            DB::rollback();
            return redirect('topup')->with('error', $ex->getMessage());
        }
    }

    public function saveTransaction($transactionDto){
        try{
            DB::beginTransaction();

            $transaction = new Transaction();
            $transaction->transaction_code = $this->utilityService->createNewTransactionNumber();
            $transaction->description = $transactionDto['description'];
            $transaction->amount = $transactionDto['amount'];
            $transaction->user_id = auth()->id();
            $transaction->save();

            // save journal to audit trail 
            $journal = new Journal();
            $journal->transaction_id = $transaction->id;
            $journal->type = 'transaction';
            $journal->description = $transactionDto['description'];
            $journal->amount = $transactionDto['amount']* -1; // because transaction is out
            $journal->user_id = auth()->id();
            $journal->save();

            $saldoUser = SaldoUser::where('user_id', auth()->id())->first();

            if(!empty($saldoUser->id)){
                $saldoUser = SaldoUser::find($saldoUser->id);
                $saldoUser->balance -=  $transactionDto['amount'];
                $saldoUser->save();
            }else{
                $saldoUser = new SaldoUser();
                $saldoUser->user_id = auth()->id();
                $saldoUser->balance = $transactionDto['amount']*-1;  // if force to add, then saldo is negative
                $saldoUser->save();
            }

            DB::commit();
            return redirect('transaction')->with('success', 'Success add transaction');
        }catch(Exception $ex){

            DB::rollback();
            return redirect('transaction')->with('error', $ex->getMessage());
        }
    }


}
