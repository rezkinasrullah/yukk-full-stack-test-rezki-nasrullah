<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthenticationRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Services\AuthenticationService;

class AuthenticationController extends Controller
{
    protected $authenticationService;

    public function __construct(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    public function guest(){
        return $this->authenticationService->guest();
    }
    public function login(){
        return view('backend.layouts.login');
    }
    public function doLogin(AuthenticationRequest $request){
        return $this->authenticationService->login($request->validated());
    }

    public function register(){
        return view('backend.layouts.register');
    }
    public function doRegister(RegisterRequest $request){
        return $this->authenticationService->register($request->validated());
    }

    public function logout(){
        return $this->authenticationService->logout();
    }
}
