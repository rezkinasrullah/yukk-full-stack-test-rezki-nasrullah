<?php

namespace App\Http\Controllers;

use App\Http\Services\TransactionService;
use App\Http\Services\UtilityService;

class DashboardController extends Controller
{
    protected $transactionServcie;
    protected $utilityService;
    public function __construct(TransactionService $transactionServcie, UtilityService $utilityService)
    {
        $this->transactionServcie = $transactionServcie;
        $this->utilityService = $utilityService;
    }

    public function index(){
        $userSaldo = $this->utilityService->numberDashboard($this->transactionServcie->getCurrentUserBalance());
        $userTopup = $this->utilityService->numberDashboard($this->transactionServcie->getCurrentUserTotalTopup());
        $userTransaction = $this->utilityService->numberDashboard($this->transactionServcie->getCurrentUserTotalTransaction());

        $userJournal = $this->transactionServcie->getCurrentUserLatestJournal();

        return view("backend.partials.dashboard", [
            "userSaldo" => $userSaldo,
            "userTopup" => $userTopup,
            "userTransaction" => $userTransaction,
            "userJournal" => $userJournal
        ]);
    }
}
