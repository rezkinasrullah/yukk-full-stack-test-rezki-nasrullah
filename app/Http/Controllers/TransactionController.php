<?php

namespace App\Http\Controllers;

use App\Http\Requests\TopupRequest;
use App\Http\Requests\TransactionRequest;
use App\Http\Services\TransactionService;
use App\Http\Services\UtilityService;

class TransactionController extends Controller
{
    protected $transactionService;


    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }


    public function history(){
        $histories = $this->transactionService->getCurrentUserHistory(request()->input('q'));
        return view('backend.partials.history.show', [
            'histories' => $histories
        ]);
    }

    public function topup(){
        $topups = $this->transactionService->getCurrentUserTopup(request()->input('q'));
        return view('backend.partials.topup.index', [
            'topups' => $topups
        ]);
    }

    public function transaction(){
        $transactions = $this->transactionService->getCurrentUserTransaction(request()->input('q'));
        return view('backend.partials.transaction.index', [
            'transactions' => $transactions
        ]);
    }

    public function topupCreate(){
        return view('backend.partials.topup.create');
    }

    public function transactionCreate(){
        $currentSaldo = $this->transactionService->getCurrentUserBalance();

        if($currentSaldo <= 0) return redirect('topup')->with('error', 'Harap topup sebelum melakukan transaksi');

        return view('backend.partials.transaction.create');

    }


    public function topupSave(TopupRequest $request){
        return $this->transactionService->saveToptup($request->validated(), $request);
    }

    public function transactionSave(TransactionRequest $request){
        return $this->transactionService->saveTransaction($request->validated());
    }


}
