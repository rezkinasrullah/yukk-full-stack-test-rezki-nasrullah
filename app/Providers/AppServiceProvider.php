<?php

namespace App\Providers;

use App\Http\Services\AuthenticationService;
use App\Http\Services\TransactionService;
use App\Http\Services\UtilityService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        // Depedency Injection, Inject this 3 classes to accross controller
        $this->app->singleton(TransactionService::class, function ($app) {
            return new TransactionService(new UtilityService);
        });

        $this->app->singleton(AuthenticationService::class, function ($app) {
            return new AuthenticationService();
        });

        $this->app->singleton(UtilityService::class, function ($app) {
            return new UtilityService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
