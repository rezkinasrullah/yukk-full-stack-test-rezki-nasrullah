<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    
    protected $table = "transaction";
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function journal()
    {
        return $this->morphOne(Journal::class, 'journalable', 'type', 'transaction_id');
    }
}
