<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaldoUser extends Model
{
    use HasFactory;
    
    protected $table = "saldo_user";

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
