<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Journal extends Model
{
    use HasFactory;
    
    protected $table = "journal";

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function journalable()
    {
        return $this->morphTo(null, 'type', 'transaction_id');
    }
}
